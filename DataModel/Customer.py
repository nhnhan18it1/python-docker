import psycopg2
import setting

conn = psycopg2.connect(
    database=setting.DB_NAME,
    user=setting.DB_USER,
    password=setting.DB_PASSWORD,
    host=setting.DB_HOST,
    port=setting.DB_PORT,
)

def addCustomer(CusName="abc",Address="abc",City="abc",Country="abc",ContactName="nhan",PostalCode="55000"):
    query = "INSERT INTO Customers (customername,contact_name,address,city,postalcode,country) VALUES (%s,%s,%s,%s,%s,%s)"
    data = (CusName,ContactName,Address,City,PostalCode,Country)
    try:
        cursor = conn.cursor()
        cursor.execute(query, data)
        conn.commit()
        cursor.close()
    except Exception as e:
        print(e)

